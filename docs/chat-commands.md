Hier findest du alle Chat Befehle, die auf DJ Bilbo's Twitch Kanal benutzt werden können.

**Bitte beachten**

* Wörter, die von geschweiften Klammern (`{` und `}`) umschlossen sind, benötigen einen Wert.  
  Beispiel: Für `{USERNAME}` wäre z.B. `@DJBilbo` bzw. `DJBilbo` möglich.
* Bei Wörter, die von eckigen Klammern (`[` und `]`) umschlossen sind, ist eine Werteingabe optional.  
  `[SECONDS]` könnte beispielsweise ausgelassen oder durch eine Sekundenangabe (z. B. `10`) ersetzt werden.
* Die Symbole selbst (`[`, `]`, `{` und `}`) dürfen bei der Befehlseingabe allerdings nicht mit ins Textfeld eingegeben werden.
* Ein Alias ist eine alternative Schreibweise für den Befehl.  
  Beispiel: Statt `!discord` kannst du auch die kurze Schreibweise `!dc` benutzen.
  

---

# Befehle für alle Benutzer
## !clap
**Syntax**  
`!clap`

---

## !dance
**Syntax**  
`!dance`

---

## !discord
**Syntax**  
`!discord`

**Aliase**  
`!dc`

---

## !djloben
**Syntax**  
`!djloben`

---

## !follow
**Syntax**  
`!follow`

---

## !following
Gibt aus, wie lange du schon DJ Bilbo folgst.

**Syntax**  
`!following`

---

## !gz
Gratuliere einem anderen Benutzer.

**Syntax**  
`!gz {USERNAME}`

| Parameter | Beschreibung |
|-|-|
| **USERNAME** | Der Benutzer, dem du gratulieren möchtest. |

---

## !hi
**Syntax**  
`!hi [USERNAME]`

---

## !lurk
Teile allen mit, dass du im Chat inaktiv bist.
Optional kannst du einen Grund mit angeben.

Sobald du wieder etwas in den Chat schreibst, wird der Bot das automatisch erkennen.

**Syntax**  
`!lurk [REASON]`

| Parameter | Beschreibung |
|-|-|
| **REASON** | Optional: Der Grund, wieso du aktuell im Chat inaktiv bist. |

---

## !modsloben
**Syntax**  
`!modsloben`

---

## !polaroid
**Syntax**  
`!polaroid`

---

## !prime
**Syntax**  
`!prime`

---

## !prost
**Syntax**  
`!prost [USERNAME]`

**Aliase**  
`!cheers`

---

## !ty
**Syntax**  
`!ty`

---

## !uptime
**Syntax**  
`!uptime`

---

## !watchtime
**Syntax**  
`!watchtime`

---

## !wb
**Syntax**  
`!wb`

---

## !whatsapp
**Syntax**  
`!whatsapp`

---

# Befehle für VIPs

---

# Befehle für Moderatoren
## !chatloben
**Syntax**  
`!chatloben`

---

## !shoutout
**Syntax**  
`!shoutout`

**Aliase**  
`!so`

---

## !welcome
**Syntax**  
`!welcome`

---

# Befehle für Administratoren
## !get_count
**Syntax**
`!get_count {TYPE}`

| Parameter | Beschreibung |
|-|-|
| **TYPE** | `djloben` oder `modsloben` |

---

## !get_points
**Syntax**
`!get_points {USERNAME}`

| Parameter | Beschreibung |
|-|-|
| **USERNAME** | Der Benutzer, dessen Punkte du überprüfen möchtest. |

---

## !set_count
**Syntax**  
`!set_count {TYPE} {AMOUNT}`

| Parameter | Beschreibung |
|-|-|
| **TYPE** | `djloben` oder `modsloben` |
| **AMOUNT** | Wert der gesetzt werden soll. |

---

## !set_points
**Syntax**
`!set_points {USERNAME} {AMOUNT}`

| Parameter | Beschreibung |
|-|-|
| **USERNAME** | Der Benutzer, dessen Punkte angepasst werden sollen. |
| **AMOUNT** | Anzahl der Punkte, die der Benutzer erhalten soll. Benutze negative Werte, falls du die Punkte des Benutzers reduzieren willst. |

---

## !user
**Syntax**  
`!user {USERNAME} {CATEGORY} {ACTION} [...]`

---
